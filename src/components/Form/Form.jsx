import React, { useState } from 'react'
import Styles from './Form.module.css'

import validator from 'validator';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Form() {

    const [userData, setUserData] = useState(
        {
            name: "",
            username: "",
            email: "",
            mobile: "",
            checkbox: false
        }
    );

    const onChangeHandler = (e) => {
        e.preventDefault();            

        
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })

        console.log(e.target.value);
    }

    const [nameError, setNameError] = useState(false);
    const [usernameError, setUsernameError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [mobileError, setMobileError] = useState(false);
    const [checkboxError, setCheckboxError] = useState(false);

    function isValid(type) {
        switch(type) {
            case 'name':
                if(userData.name === "" || userData.name.length < 3){
                    setNameError(true);
                    return false;
                }else{  
                    setNameError(false);
                    return true;
                }
            case 'username':
                if(userData.username === "" || userData.username.length < 3){
                    setUsernameError(true);
                    return false;
                }
                else{
                    setUsernameError(false);
                    return true;
                }
            case 'email':
                if(userData.email === "" || !validator.isEmail(userData.email)){
                    setEmailError(true);
                    return false;
                }
                else{
                    setEmailError(false);
                    return true;
                }
            case 'mobile':
                if(userData.mobile.length !== 10 || !validator.isMobilePhone(userData.mobile)){
                    setMobileError(true);
                    return false;
                }
                else{
                    setMobileError(false);
                    return true;
                }
            case 'checkbox':
                if(userData.checkbox === false){
                    setCheckboxError(true);
                    return false;
                }
                else{
                    setCheckboxError(false);
                    return true;
                }
            default:
                console.log("Invalid type");
            break;
        }
    }

    function onSuccess(){
        toast.success("Form Submitted Successfully", {
            position: "top-right",
            autoClose: 2500,
            closeOnClick: true,
            pauseOnHover: true,
            theme: "dark"
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if(isValid('name') && isValid('username') && isValid('email') && isValid('mobile') && isValid('checkbox')
        ){  
            onSuccess();
            console.log("Form submitted");
        }
        else{
            console.log("Form not submitted");
        }

        console.log(userData);
    }


    return (
        <div>
            <form className={Styles.form_container} onSubmit={(e)=> e.preventDefault()}>
                <div className={Styles.input_container}>
                <input 
                    className={Styles.input_box} 
                    type="text" 
                    name='name'
                    placeholder="Name" 
                    onChange={onChangeHandler} 
                    required
                />
                {nameError && <span className={Styles.error}>Name is required</span>}
                </div>
                
                <div className={Styles.input_container}>

                <input 
                    className={Styles.input_box} 
                    type="text" 
                    name='username'
                    placeholder="UserName" 
                    onChange={onChangeHandler} 
                    required
                />
                {usernameError && <span className={Styles.error}>UserName is required</span>}
                </div>

                <div className={Styles.input_container} >

                <input 
                    className={Styles.input_box} 
                    type="email" 
                    name='email'
                    placeholder="Email" 
                    onChange={onChangeHandler} 
                    required
                />
                {emailError && <span className={Styles.error}>Enter Valid Email Id</span>}
                </div>

                <div className={Styles.input_container}>

                <input 
                    className={Styles.input_box} 
                    type="string" 
                    name='mobile'
                    placeholder="Mobile" 
                    onChange={onChangeHandler} 
                    required
                />
                {mobileError && <span className={Styles.error}>Enter Valid Mobile Number</span>}
                </div>
                
                <div className={Styles.input_container}>

                <div className={Styles.checkbox_container}>
                    <input 
                        type="checkbox" 
                        name="checkbox" 
                        id="checkbox_id" 
                        onChange={(e) => {
                            setUserData({
                                ...userData,
                                [e.target.name]: e.target.checked
                            })
                            console.log(e.target.checked);
                        }}
                    />
                    <label>Share my registration data with Superapp</label>
                </div>
                {checkboxError && <span className={Styles.error}>Checkbox is required ⚠️</span>}
                </div>
                
                <button className={Styles.submit_btn} onClick={handleSubmit}>
                    SIGN UP
                </button>

                <div className={Styles.footer}>
                    <p>By clicking on Sign up. you agree to Superapp <span>Terms and Conditions of Use</span></p>

                    <p>To learn more about how Superapp collects, uses, shares and protects your personal data please head Superapp <span>Privacy Policy</span></p>
                </div>

            </form>
            <ToastContainer/>
        </div>
    )
}

export default Form;