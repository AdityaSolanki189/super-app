import Styles from './Register.module.css'
import Form from '../../components/Form/Form'

function registerPage() {
    return (
        <section className={Styles.container}>
            
            <div className={Styles.left_container}>
                <div className={Styles.header}>
                    <div className={Styles.header_text}>
                        <p> Already have an Account? </p>
                        <button className={Styles.login_btn}>
                            LOGIN
                        </button>
                        
                    </div>
                </div>
                <div className={Styles.footer}>
                    <p> Discover new things on <span>
                        Superapp </span></p>
                </div>
            </div>

            <div className={Styles.right_container}>
                <div className={Styles.header}>
                    <span className={Styles.title}>
                        Super App
                    </span>
                    <span className={Styles.text}>
                        Create your new Account
                    </span>
                    <span className={Styles.option}>
                        Email<span> | </span>Google
                    </span>
                </div>

                <div className={Styles.form}>
                    <Form />
                </div>
            </div>

        </section>
    )
}

export default registerPage;