import React from 'react';
import { Route, Routes } from 'react-router-dom';

import RegisterPage from './layouts/Register/Register';
import CategoryPage from './layouts/Category/Category';

function App() {
    return (
        <div>
            <Routes>
                <Route path="/" element={<RegisterPage />} />
                <Route path="/category" element={<CategoryPage />} />
            </Routes>
        </div>
    );
}

export default App;